## 0. Activate pGlyco
If pGlyco3 is not activated, double click __register.bat. Fill in the forms, and then click "Copy to clipboard" button in the dialog, then paste the information into the email and send it to us (pglyco@ict.ac.cn).

## 1. Convert N to J
Convert N with motif (N-X-S/T/C) into J in a fasta file if necessary. Run:
```
cd bin
FastaNGlysiteN2J.exe "C:\your\fastafile.fasta" STC
cd ..
```
The output is a new fasta file "C:\your\fastafile.fasta.N2J"

## 2. Modify parameter.cfg
Configure the parameter.cfg. A parameter template has been given in this folder, please read "param_template.cfg" carefully for details. You'd better copy this template file in to your experiment project folder, and modify the copied one.

## 3. Run pGlyco
```
run_pGlyco.bat C:\your\project\path\parameter.cfg
```

## 4. Multi-process run
```
MultiProcess.exe pGlyco 3 run_pGlyco.bat C:\your\project\path\parameter.cfg
# -- here 3 is the process number
```
or right click to "edit" multiprocess_easy.bat and double click it to run.


## Cite us
pGlyco3 paper is not ready yet, you can cite pGlyco2 paper instead.